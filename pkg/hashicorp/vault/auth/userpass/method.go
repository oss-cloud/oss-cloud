package userpass

import (
	"context"

	"github.com/hashicorp/vault/api"
	"gitlab.com/oss-cloud/oss-cloud/pkg/hashicorp/vault/auth"
	"go.opentelemetry.io/otel"
)

var tracer = otel.Tracer("vault/auth/userpass")

var _ auth.Method = &authMethod{}

type authMethod struct {
	auth.BaseMethod
	Username string `json:"-"`
	Password string `json:"password"`
}

func New(path, username, password string) auth.Method {
	return &authMethod{
		BaseMethod: auth.BaseMethod{
			Name: path,
		},
		Username: username,
		Password: password,
	}
}

func (a *authMethod) InitialToken(ctx context.Context, client *api.Client) (token string, err error) {
	_, span := tracer.Start(ctx, "InitialToken")
	defer span.End()

	secret, err := a.BaseMethod.Write(ctx, client, a)
	if err != nil {
		return
	}
	if secret == nil {
		return "", auth.ErrNotExists
	}
	return secret.TokenID()
}
