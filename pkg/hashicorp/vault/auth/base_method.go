package auth

import (
	"context"
	"io"
	"net/http"

	"github.com/hashicorp/vault/api"
	"go.opentelemetry.io/otel"
)

var tracer = otel.Tracer("vault-auth")

type BaseMethod struct {
	Name string `json:"-"`
}

func (m *BaseMethod) Write(ctx context.Context, client *api.Client, data interface{}) (*api.Secret, error) {
	ctx, span := tracer.Start(ctx, "Write")
	defer span.End()

	r := client.NewRequest(http.MethodPost, "/v1/auth/"+m.Name+"/login")
	if err := r.SetJSONBody(data); err != nil {
		return nil, err
	}

	resp, err := client.RawRequestWithContext(ctx, r)
	if resp != nil {
		defer resp.Body.Close()
	}
	if resp != nil && resp.StatusCode == 404 {
		secret, parseErr := api.ParseSecret(resp.Body)
		switch parseErr {
		case nil:
		case io.EOF:
			return nil, nil
		default:
			return nil, err
		}
		if secret != nil && (len(secret.Warnings) > 0 || len(secret.Data) > 0) {
			return secret, err
		}
	}
	if err != nil {
		return nil, err
	}

	return api.ParseSecret(resp.Body)
}
