package auth

import "errors"

var (
	ErrNotExists = errors.New("auth: not exists")
)
