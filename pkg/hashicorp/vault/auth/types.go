package auth

import (
	"context"

	"github.com/hashicorp/vault/api"
)

type Method interface {
	InitialToken(ctx context.Context, client *api.Client) (string, error)
}
