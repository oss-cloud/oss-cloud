package homedir

import (
	"bytes"
	"os/exec"
	"strings"
)

func homeCmd(stdout *bytes.Buffer) (done bool, dir string, err error) {
	cmd := exec.Command("sh", "-c", `dscl -q . -read /Users/"$(whoami)" NFSHomeDirectory | sed 's/^[^ ]*: //'`)
	cmd.Stdout = stdout
	if err := cmd.Run(); err == nil {
		result := strings.TrimSpace(stdout.String())
		if result != "" {
			return true, result, nil
		}
	}
	return false, "", nil
}
