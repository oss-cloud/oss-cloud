// +build !darwin !windows

package homedir

import (
	"bytes"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func homeCmd(stdout *bytes.Buffer) (done bool, dir string, err error) {
	cmd := exec.Command("getent", "passwd", strconv.Itoa(os.Getuid()))
	cmd.Stdout = stdout
	if err := cmd.Run(); err != nil {
		// If the error is ErrNotFound, we ignore it. Otherwise, return it.
		if err != exec.ErrNotFound {
			return true, "", err
		}
	} else {
		if passwd := strings.TrimSpace(stdout.String()); passwd != "" {
			// username:password:uid:gid:gecos:home:shell
			passwdParts := strings.SplitN(passwd, ":", 7)
			if len(passwdParts) > 5 {
				return true, passwdParts[5], nil
			}
		}
	}

	return false, "", err
}
