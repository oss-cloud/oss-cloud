package otsql

import (
	"context"
	"database/sql/driver"

	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/label"
	"go.opentelemetry.io/otel/trace"
)

var (
	attrMissingContext = label.String("ocsql.warning", "missing upstream context")
)

func wrapConn(parent driver.Conn, options TraceOptions) driver.Conn {
	var (
		n, hasNameValueChecker = parent.(driver.NamedValueChecker)
		s, hasSessionResetter  = parent.(driver.SessionResetter)
	)
	c := &ocConn{parent: parent, options: options}
	switch {
	case !hasNameValueChecker && !hasSessionResetter:
		return c
	case hasNameValueChecker && !hasSessionResetter:
		return struct {
			conn
			driver.NamedValueChecker
		}{c, n}
	case !hasNameValueChecker && hasSessionResetter:
		return struct {
			conn
			driver.SessionResetter
		}{c, s}
	case hasNameValueChecker && hasSessionResetter:
		return struct {
			conn
			driver.NamedValueChecker
			driver.SessionResetter
		}{c, n, s}
	}
	panic("unreachable")
}

type conn interface {
	driver.Pinger
	driver.Execer
	driver.ExecerContext
	driver.Queryer
	driver.QueryerContext
	driver.Conn
	driver.ConnPrepareContext
	driver.ConnBeginTx
}

var (
	_ conn                     = &ocConn{}
	_ driver.NamedValueChecker = &ocConn{}
)

// ocConn implements driver.Conn
type ocConn struct {
	parent  driver.Conn
	options TraceOptions
}

func (c ocConn) Ping(ctx context.Context) (err error) {
	onDeferWithErr := recordCallStats(ctx, "go.sql.ping", c.options.InstanceName)
	defer func() {
		// Invoking this function in a defer so that we can capture
		// the value of err as set on function exit.
		onDeferWithErr(err)
	}()

	if c.options.Ping && (c.options.AllowRoot || trace.SpanFromContext(ctx) != nil) {
		var span trace.Span
		ctx, span = tracer.Start(ctx, "sql:ping",
			trace.WithSpanKind(trace.SpanKindClient),
		)
		if len(c.options.DefaultAttributes) > 0 {
			span.SetAttributes(c.options.DefaultAttributes...)
		}
		defer func() {
			if err != nil {
				span.SetStatus(codes.Error, err.Error())
			} else {
				span.SetStatus(codes.Ok, "Ok")
			}
			span.End()
		}()
	}

	if pinger, ok := c.parent.(driver.Pinger); ok {
		err = pinger.Ping(ctx)
	}
	return
}

func (c ocConn) Exec(query string, args []driver.Value) (res driver.Result, err error) {
	onDeferWithErr := recordCallStats(context.Background(), "go.sql.exec", c.options.InstanceName)
	defer func() {
		// Invoking this function in a defer so that we can capture
		// the value of err as set on function exit.
		onDeferWithErr(err)
	}()

	if exec, ok := c.parent.(driver.Execer); ok {
		if !c.options.AllowRoot {
			return exec.Exec(query, args)
		}

		ctx, span := tracer.Start(context.Background(), "sql:exec",
			trace.WithSpanKind(trace.SpanKindClient),
		)
		attrs := make([]label.KeyValue, 0, len(c.options.DefaultAttributes)+2)
		attrs = append(attrs, c.options.DefaultAttributes...)
		attrs = append(
			attrs,
			label.String(
				"ocsql.deprecated", "driver does not support ExecerContext",
			),
		)
		if c.options.Query {
			attrs = append(attrs, label.String("sql.query", query))
			if c.options.QueryParams {
				attrs = append(attrs, paramsAttr(args)...)
			}
		}
		span.SetAttributes(attrs...)

		defer func() {
			setSpanStatus(span, c.options, err)
			span.End()
		}()

		if res, err = exec.Exec(query, args); err != nil {
			return nil, err
		}

		return ocResult{parent: res, ctx: ctx, options: c.options}, nil
	}

	return nil, driver.ErrSkip
}

func (c ocConn) ExecContext(ctx context.Context, query string, args []driver.NamedValue) (res driver.Result, err error) {
	onDeferWithErr := recordCallStats(ctx, "go.sql.exec", c.options.InstanceName)
	defer func() {
		// Invoking this function in a defer so that we can capture
		// the value of err as set on function exit.
		onDeferWithErr(err)
	}()

	if execCtx, ok := c.parent.(driver.ExecerContext); ok {
		parentSpan := trace.SpanFromContext(ctx)
		if !c.options.AllowRoot && parentSpan == nil {
			return execCtx.ExecContext(ctx, query, args)
		}

		var span trace.Span
		if parentSpan == nil {
			ctx, span = tracer.Start(ctx, "sql:exec",
				trace.WithSpanKind(trace.SpanKindClient),
			)
		} else {
			_, span = tracer.Start(ctx, "sql:exec",
				trace.WithSpanKind(trace.SpanKindClient),
			)
		}
		attrs := append([]label.KeyValue(nil), c.options.DefaultAttributes...)
		if c.options.Query {
			attrs = append(attrs, label.String("sql.query", query))
			if c.options.QueryParams {
				attrs = append(attrs, namedParamsAttr(args)...)
			}
		}
		span.SetAttributes(attrs...)

		defer func() {
			setSpanStatus(span, c.options, err)
			span.End()
		}()

		if res, err = execCtx.ExecContext(ctx, query, args); err != nil {
			return nil, err
		}

		return ocResult{parent: res, ctx: ctx, options: c.options}, nil
	}

	return nil, driver.ErrSkip
}

func (c ocConn) Query(query string, args []driver.Value) (rows driver.Rows, err error) {
	onDeferWithErr := recordCallStats(context.Background(), "go.sql.query", c.options.InstanceName)
	defer func() {
		// Invoking this function in a defer so that we can capture
		// the value of err as set on function exit.
		onDeferWithErr(err)
	}()

	if queryer, ok := c.parent.(driver.Queryer); ok {
		if !c.options.AllowRoot {
			return queryer.Query(query, args)
		}

		ctx, span := tracer.Start(context.Background(), "sql:query",
			trace.WithSpanKind(trace.SpanKindClient),
		)
		attrs := make([]label.KeyValue, 0, len(c.options.DefaultAttributes)+2)
		attrs = append(attrs, c.options.DefaultAttributes...)
		attrs = append(
			attrs,
			label.String(
				"ocsql.deprecated", "driver does not support QueryerContext",
			),
		)
		if c.options.Query {
			attrs = append(attrs, label.String("sql.query", query))
			if c.options.QueryParams {
				attrs = append(attrs, paramsAttr(args)...)
			}
		}
		span.SetAttributes(attrs...)

		defer func() {
			setSpanStatus(span, c.options, err)
			span.End()
		}()

		rows, err = queryer.Query(query, args)
		if err != nil {
			return nil, err
		}

		return wrapRows(ctx, rows, c.options), nil
	}

	return nil, driver.ErrSkip
}

func (c ocConn) QueryContext(ctx context.Context, query string, args []driver.NamedValue) (rows driver.Rows, err error) {
	onDeferWithErr := recordCallStats(ctx, "go.sql.query", c.options.InstanceName)
	defer func() {
		// Invoking this function in a defer so that we can capture
		// the value of err as set on function exit.
		onDeferWithErr(err)
	}()

	if queryerCtx, ok := c.parent.(driver.QueryerContext); ok {
		parentSpan := trace.SpanFromContext(ctx)
		if !c.options.AllowRoot && parentSpan == nil {
			return queryerCtx.QueryContext(ctx, query, args)
		}

		var span trace.Span
		if parentSpan == nil {
			ctx, span = tracer.Start(ctx, "sql:query",
				trace.WithSpanKind(trace.SpanKindClient),
			)
		} else {
			_, span = tracer.Start(ctx, "sql:query",
				trace.WithSpanKind(trace.SpanKindClient),
			)
		}
		attrs := append([]label.KeyValue(nil), c.options.DefaultAttributes...)
		if c.options.Query {
			attrs = append(attrs, label.String("sql.query", query))
			if c.options.QueryParams {
				attrs = append(attrs, namedParamsAttr(args)...)
			}
		}
		span.SetAttributes(attrs...)

		defer func() {
			setSpanStatus(span, c.options, err)
			span.End()
		}()

		rows, err = queryerCtx.QueryContext(ctx, query, args)
		if err != nil {
			return nil, err
		}

		return wrapRows(ctx, rows, c.options), nil
	}

	return nil, driver.ErrSkip
}

func (c ocConn) Prepare(query string) (stmt driver.Stmt, err error) {
	onDeferWithErr := recordCallStats(context.Background(), "go.sql.prepare", c.options.InstanceName)
	defer func() {
		// Invoking this function in a defer so that we can capture
		// the value of err as set on function exit.
		onDeferWithErr(err)
	}()

	if c.options.AllowRoot {
		_, span := tracer.Start(context.Background(), "sql:prepare",
			trace.WithSpanKind(trace.SpanKindClient),
		)
		attrs := make([]label.KeyValue, 0, len(c.options.DefaultAttributes)+1)
		attrs = append(attrs, c.options.DefaultAttributes...)
		attrs = append(attrs, attrMissingContext)
		if c.options.Query {
			attrs = append(attrs, label.String("sql.query", query))
		}
		span.SetAttributes(attrs...)

		defer func() {
			setSpanStatus(span, c.options, err)
			span.End()
		}()
	}

	stmt, err = c.parent.Prepare(query)
	if err != nil {
		return nil, err
	}

	stmt = wrapStmt(stmt, query, c.options)
	return
}

func (c *ocConn) Close() error {
	return c.parent.Close()
}

func (c *ocConn) Begin() (driver.Tx, error) {
	return c.BeginTx(context.TODO(), driver.TxOptions{})
}

func (c *ocConn) PrepareContext(ctx context.Context, query string) (stmt driver.Stmt, err error) {
	onDeferWithErr := recordCallStats(ctx, "go.sql.prepare", c.options.InstanceName)
	defer func() {
		// Invoking this function in a defer so that we can capture
		// the value of err as set on function exit.
		onDeferWithErr(err)
	}()

	var span trace.Span
	attrs := append([]label.KeyValue(nil), c.options.DefaultAttributes...)
	if c.options.AllowRoot || trace.SpanFromContext(ctx) != nil {
		ctx, span = tracer.Start(ctx, "sql:prepare",
			trace.WithSpanKind(trace.SpanKindClient),
		)
		if c.options.Query {
			attrs = append(attrs, label.String("sql.query", query))
		}
		defer func() {
			setSpanStatus(span, c.options, err)
			span.End()
		}()
	}

	if prepCtx, ok := c.parent.(driver.ConnPrepareContext); ok {
		stmt, err = prepCtx.PrepareContext(ctx, query)
	} else {
		if span != nil {
			attrs = append(attrs, attrMissingContext)
		}
		stmt, err = c.parent.Prepare(query)
	}
	span.SetAttributes(attrs...)
	if err != nil {
		return nil, err
	}

	stmt = wrapStmt(stmt, query, c.options)
	return
}

func (c *ocConn) BeginTx(ctx context.Context, opts driver.TxOptions) (tx driver.Tx, err error) {
	onDeferWithErr := recordCallStats(ctx, "go.sql.begin", c.options.InstanceName)
	defer func() {
		// Invoking this function in a defer so that we can capture
		// the value of err as set on function exit.
		onDeferWithErr(err)
	}()

	if !c.options.AllowRoot && trace.SpanFromContext(ctx) == nil {
		if connBeginTx, ok := c.parent.(driver.ConnBeginTx); ok {
			return connBeginTx.BeginTx(ctx, opts)
		}
		return c.parent.Begin()
	}

	var span trace.Span
	attrs := append([]label.KeyValue(nil), c.options.DefaultAttributes...)

	if ctx == nil || ctx == context.TODO() {
		ctx = context.Background()
		_, span = tracer.Start(ctx, "sql:begin_transaction",
			trace.WithSpanKind(trace.SpanKindClient),
		)
		attrs = append(attrs, attrMissingContext)
	} else {
		_, span = tracer.Start(ctx, "sql:begin_transaction",
			trace.WithSpanKind(trace.SpanKindClient),
		)
	}
	defer func() {
		if len(attrs) > 0 {
			span.SetAttributes(attrs...)
		}
		span.End()
	}()

	if connBeginTx, ok := c.parent.(driver.ConnBeginTx); ok {
		tx, err = connBeginTx.BeginTx(ctx, opts)
		setSpanStatus(span, c.options, err)
		if err != nil {
			return nil, err
		}
		return ocTx{parent: tx, ctx: ctx, options: c.options}, nil
	}

	attrs = append(
		attrs,
		label.String(
			"ocsql.deprecated", "driver does not support ConnBeginTx",
		),
	)
	tx, err = c.parent.Begin()
	setSpanStatus(span, c.options, err)
	if err != nil {
		return nil, err
	}
	return ocTx{parent: tx, ctx: ctx, options: c.options}, nil
}

func (c *ocConn) CheckNamedValue(nv *driver.NamedValue) (err error) {
	nvc, ok := c.parent.(driver.NamedValueChecker)
	if ok {
		return nvc.CheckNamedValue(nv)
	}
	nv.Value, err = driver.DefaultParameterConverter.ConvertValue(nv.Value)
	return err
}
