package otsql

import (
	"context"
	"database/sql/driver"
	"fmt"
	"strconv"

	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/label"
	"go.opentelemetry.io/otel/trace"
)

func paramsAttr(args []driver.Value) []label.KeyValue {
	attrs := make([]label.KeyValue, 0, len(args))
	for i, arg := range args {
		key := "sql.arg" + strconv.Itoa(i)
		attrs = append(attrs, argToAttr(key, arg))
	}
	return attrs
}

func argToAttr(key string, val interface{}) label.KeyValue {
	switch v := val.(type) {
	case nil:
		return label.String(key, "")
	case int64:
		return label.Int64(key, v)
	case float64:
		return label.Float64(key, v)
	case bool:
		return label.Bool(key, v)
	case []byte:
		if len(v) > 256 {
			v = v[0:256]
		}
		return label.String(key, fmt.Sprintf("%s", v))
	default:
		s := fmt.Sprintf("%v", v)
		if len(s) > 256 {
			s = s[0:256]
		}
		return label.String(key, s)
	}
}

func setSpanStatus(span trace.Span, opts TraceOptions, err error) {
	var status codes.Code
	switch err {
	case nil:
		status = codes.Ok
		span.SetStatus(status, "OK")
		return
	case driver.ErrSkip:
		status = codes.Error
		if opts.DisableErrSkip {
			// Suppress driver.ErrSkip since at runtime some drivers might not have
			// certain features, and an error would pollute many spans.
			status = codes.Ok
		}
	default:
		status = codes.Error
	}
	span.SetStatus(status, err.Error())
}

func namedParamsAttr(args []driver.NamedValue) []label.KeyValue {
	attrs := make([]label.KeyValue, 0, len(args))
	for _, arg := range args {
		var key string
		if arg.Name != "" {
			key = arg.Name
		} else {
			key = "sql.arg." + strconv.Itoa(arg.Ordinal)
		}
		attrs = append(attrs, argToAttr(key, arg.Value))
	}
	return attrs
}

// wrapRows returns a struct which conforms to the driver.Rows interface.
// ocRows implements all enhancement interfaces that have no effect on
// sql/database logic in case the underlying parent implementation lacks them.
// Currently the one exception is RowsColumnTypeScanType which does not have a
// valid zero value. This interface is tested for and only enabled in case the
// parent implementation supports it.
func wrapRows(ctx context.Context, parent driver.Rows, options TraceOptions) driver.Rows {
	var (
		ts, hasColumnTypeScan = parent.(driver.RowsColumnTypeScanType)
	)

	r := ocRows{
		parent:  parent,
		ctx:     ctx,
		options: options,
	}

	if hasColumnTypeScan {
		return struct {
			ocRows
			withRowsColumnTypeScanType
		}{r, ts}
	}

	return r
}
