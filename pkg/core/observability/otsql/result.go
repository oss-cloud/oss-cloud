package otsql

import (
	"context"
	"database/sql/driver"

	"go.opentelemetry.io/otel/trace"
)

var _ driver.Result = &ocResult{}

// ocResult implements driver.Result
type ocResult struct {
	parent  driver.Result
	ctx     context.Context
	options TraceOptions
}

func (r ocResult) LastInsertId() (id int64, err error) {
	if r.options.LastInsertID {
		_, span := tracer.Start(r.ctx, "sql:last_insert_id",
			trace.WithSpanKind(trace.SpanKindClient),
		)
		if len(r.options.DefaultAttributes) > 0 {
			span.SetAttributes(r.options.DefaultAttributes...)
		}
		defer func() {
			setSpanStatus(span, r.options, err)
			span.End()
		}()
	}

	id, err = r.parent.LastInsertId()
	return
}

func (r ocResult) RowsAffected() (cnt int64, err error) {
	if r.options.RowsAffected {
		_, span := tracer.Start(r.ctx, "sql:rows_affected",
			trace.WithSpanKind(trace.SpanKindClient),
		)
		if len(r.options.DefaultAttributes) > 0 {
			span.SetAttributes(r.options.DefaultAttributes...)
		}
		defer func() {
			setSpanStatus(span, r.options, err)
			span.End()
		}()
	}

	cnt, err = r.parent.RowsAffected()
	return
}
