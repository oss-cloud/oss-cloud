package otsql

import (
	"context"
	"time"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/label"
	"go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/unit"
)

var (
	meter  = otel.Meter("otel/otsql")
	tracer = otel.Tracer("otel/otsql")
)

const (
	// GoSQLInstance is the SQL instance name.
	GoSQLInstance = "go_sql_instance"
	// GoSQLMethod is the SQL method called.
	GoSQLMethod = "go_sql_method"
	// GoSQLError is the error received while calling a SQL method.
	GoSQLError = "go_sql_error"
	// GoSQLStatus identifies success vs. error from the SQL method response.
	GoSQLStatus = "go_sql_status"
)

var (
	valueOK  = label.String(GoSQLStatus, "OK")
	valueErr = label.String(GoSQLStatus, "ERROR")
)

var (
	MeasureLatencyMs, _ = meter.NewFloat64ValueRecorder(
		"go.sql/latency",
		metric.WithDescription("The latency of calls in milliseconds"),
		metric.WithUnit(unit.Milliseconds),
	)
)

func recordCallStats(ctx context.Context, method, instanceName string) func(err error) {
	var tags []label.KeyValue
	startTime := time.Now()

	return func(err error) {
		timeSpentMs := float64(time.Since(startTime).Nanoseconds()) / 1e6

		if err != nil {
			tags = []label.KeyValue{
				label.String(GoSQLMethod, method),
				valueErr,
				label.String(GoSQLError, err.Error()),
				label.String(GoSQLInstance, instanceName),
			}
		} else {
			tags = []label.KeyValue{
				label.String(GoSQLMethod, method),
				valueOK,
				label.String(GoSQLInstance, instanceName),
			}
		}

		MeasureLatencyMs.Record(ctx, timeSpentMs, tags...)
	}
}
