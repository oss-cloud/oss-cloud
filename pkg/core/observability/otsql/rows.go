package otsql

import (
	"context"
	"database/sql/driver"
	"io"
	"reflect"

	"go.opentelemetry.io/otel/trace"
)

var (
	_ driver.Rows                           = &ocRows{}
	_ driver.RowsNextResultSet              = &ocRows{}
	_ driver.RowsColumnTypeDatabaseTypeName = &ocRows{}
	_ driver.RowsColumnTypeLength           = &ocRows{}
	_ driver.RowsColumnTypeNullable         = &ocRows{}
	_ driver.RowsColumnTypePrecisionScale   = &ocRows{}
)

// withRowsColumnTypeScanType is the same as the driver.RowsColumnTypeScanType
// interface except it omits the driver.Rows embedded interface.
// If the original driver.Rows implementation wrapped by ocsql supports
// RowsColumnTypeScanType we enable the original method implementation in the
// returned driver.Rows from wrapRows by doing a composition with ocRows.
type withRowsColumnTypeScanType interface {
	ColumnTypeScanType(index int) reflect.Type
}

// ocRows implements driver.Rows and all enhancement interfaces except
// driver.RowsColumnTypeScanType.
type ocRows struct {
	parent  driver.Rows
	ctx     context.Context
	options TraceOptions
}

// HasNextResultSet calls the implements the driver.RowsNextResultSet for ocRows.
// It returns the the underlying result of HasNextResultSet from the ocRows.parent
// if the parent implements driver.RowsNextResultSet.
func (r ocRows) HasNextResultSet() bool {
	if v, ok := r.parent.(driver.RowsNextResultSet); ok {
		return v.HasNextResultSet()
	}

	return false
}

// NextResultsSet calls the implements the driver.RowsNextResultSet for ocRows.
// It returns the the underlying result of NextResultSet from the ocRows.parent
// if the parent implements driver.RowsNextResultSet.
func (r ocRows) NextResultSet() error {
	if v, ok := r.parent.(driver.RowsNextResultSet); ok {
		return v.NextResultSet()
	}

	return io.EOF
}

// ColumnTypeDatabaseTypeName calls the implements the driver.RowsColumnTypeDatabaseTypeName for ocRows.
// It returns the the underlying result of ColumnTypeDatabaseTypeName from the ocRows.parent
// if the parent implements driver.RowsColumnTypeDatabaseTypeName.
func (r ocRows) ColumnTypeDatabaseTypeName(index int) string {
	if v, ok := r.parent.(driver.RowsColumnTypeDatabaseTypeName); ok {
		return v.ColumnTypeDatabaseTypeName(index)
	}

	return ""
}

// ColumnTypeLength calls the implements the driver.RowsColumnTypeLength for ocRows.
// It returns the the underlying result of ColumnTypeLength from the ocRows.parent
// if the parent implements driver.RowsColumnTypeLength.
func (r ocRows) ColumnTypeLength(index int) (length int64, ok bool) {
	if v, ok := r.parent.(driver.RowsColumnTypeLength); ok {
		return v.ColumnTypeLength(index)
	}

	return 0, false
}

// ColumnTypeNullable calls the implements the driver.RowsColumnTypeNullable for ocRows.
// It returns the the underlying result of ColumnTypeNullable from the ocRows.parent
// if the parent implements driver.RowsColumnTypeNullable.
func (r ocRows) ColumnTypeNullable(index int) (nullable, ok bool) {
	if v, ok := r.parent.(driver.RowsColumnTypeNullable); ok {
		return v.ColumnTypeNullable(index)
	}

	return false, false
}

// ColumnTypePrecisionScale calls the implements the driver.RowsColumnTypePrecisionScale for ocRows.
// It returns the the underlying result of ColumnTypePrecisionScale from the ocRows.parent
// if the parent implements driver.RowsColumnTypePrecisionScale.
func (r ocRows) ColumnTypePrecisionScale(index int) (precision, scale int64, ok bool) {
	if v, ok := r.parent.(driver.RowsColumnTypePrecisionScale); ok {
		return v.ColumnTypePrecisionScale(index)
	}

	return 0, 0, false
}

func (r ocRows) Columns() []string {
	return r.parent.Columns()
}

func (r ocRows) Close() (err error) {
	if r.options.RowsClose {
		_, span := tracer.Start(r.ctx, "sql:rows_close",
			trace.WithSpanKind(trace.SpanKindClient),
		)
		if len(r.options.DefaultAttributes) > 0 {
			span.SetAttributes(r.options.DefaultAttributes...)
		}
		defer func() {
			setSpanStatus(span, r.options, err)
			span.End()
		}()
	}

	err = r.parent.Close()
	return
}

func (r ocRows) Next(dest []driver.Value) (err error) {
	if r.options.RowsNext {
		_, span := tracer.Start(r.ctx, "sql:rows_next",
			trace.WithSpanKind(trace.SpanKindClient),
		)
		if len(r.options.DefaultAttributes) > 0 {
			span.SetAttributes(r.options.DefaultAttributes...)
		}
		defer func() {
			if err == io.EOF {
				// not an error; expected to happen during iteration
				setSpanStatus(span, r.options, nil)
			} else {
				setSpanStatus(span, r.options, err)
			}
			span.End()
		}()
	}

	err = r.parent.Next(dest)
	return
}
