package otsql

import (
	"context"
	"database/sql/driver"

	"go.opentelemetry.io/otel/trace"
)

var _ driver.Tx = ocTx{}

// ocTx implements driver.Tx
type ocTx struct {
	parent  driver.Tx
	ctx     context.Context
	options TraceOptions
}

func (t ocTx) Commit() (err error) {
	onDeferWithErr := recordCallStats(context.Background(), "go.sql.commit", t.options.InstanceName)
	defer func() {
		// Invoking this function in a defer so that we can capture
		// the value of err as set on function exit.
		onDeferWithErr(err)
	}()

	_, span := tracer.Start(t.ctx, "sql:commit",
		trace.WithSpanKind(trace.SpanKindClient),
	)
	if len(t.options.DefaultAttributes) > 0 {
		span.SetAttributes(t.options.DefaultAttributes...)
	}
	defer func() {
		setSpanStatus(span, t.options, err)
		span.End()
	}()

	err = t.parent.Commit()
	return
}

func (t ocTx) Rollback() (err error) {
	onDeferWithErr := recordCallStats(context.Background(), "go.sql.rollback", t.options.InstanceName)
	defer func() {
		// Invoking this function in a defer so that we can capture
		// the value of err as set on function exit.
		onDeferWithErr(err)
	}()

	_, span := tracer.Start(t.ctx, "sql:rollback",
		trace.WithSpanKind(trace.SpanKindClient),
	)
	if len(t.options.DefaultAttributes) > 0 {
		span.SetAttributes(t.options.DefaultAttributes...)
	}
	defer func() {
		setSpanStatus(span, t.options, err)
		span.End()
	}()

	err = t.parent.Rollback()
	return
}
