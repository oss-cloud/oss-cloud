package otsql

import (
	"context"
	"database/sql/driver"

	"go.opentelemetry.io/otel/label"
)

var _ driver.Connector = &otConnector{}

type otConnector struct {
	parent    driver.Driver
	connector driver.Connector
	option    TraceOptions
}

func WrapConnector(base driver.Connector, option *TraceOptions) driver.Connector {
	var o TraceOptions
	o = *option
	if o.InstanceName == "" {
		o.InstanceName = defaultInstanceName
	} else {
		o.DefaultAttributes = append(o.DefaultAttributes, label.String("sql.instance", o.InstanceName))
	}

	return &otConnector{
		parent:    base.Driver(),
		connector: base,
		option:    o,
	}
}

// Open implements driver.Driver
func (d otConnector) Open(name string) (driver.Conn, error) {
	c, err := d.parent.Open(name)
	if err != nil {
		return nil, err
	}
	return wrapConn(c, d.option), nil
}

func (d otConnector) Driver() driver.Driver {
	return d
}

func (d *otConnector) Connect(ctx context.Context) (driver.Conn, error) {
	c, err := d.connector.Connect(ctx)
	if err != nil {
		return nil, err
	}
	return &ocConn{parent: c, options: d.option}, nil
}
