package pool

import "time"

type Options struct {
	ShutdownTimeout time.Duration
}

func ExtendDefaultOptions(opts ...Option) *Options {
	var opt = &Options{
		ShutdownTimeout: 20 * time.Second,
	}
	for _, o := range opts {
		o(opt)
	}

	return opt
}

type Option func(*Options)

func WithShutdownTimeout(d time.Duration) Option {
	return func(options *Options) {
		options.ShutdownTimeout = d
	}
}
