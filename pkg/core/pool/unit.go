package pool

import (
	"context"
	"net/http"
)

type Unit interface {
	Start(ctx context.Context) error
	Stop(ctx context.Context) error
}

func ServerUnit(server *http.Server) Unit {
	return &serverUnit{
		server: server,
	}
}

type serverUnit struct {
	server *http.Server
}

func (u *serverUnit) Start(_ context.Context) error {
	return u.server.ListenAndServe()
}

func (u *serverUnit) Stop(ctx context.Context) error {
	return u.server.Shutdown(ctx)
}
