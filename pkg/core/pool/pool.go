package pool

import (
	"context"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/metric"
)

var meter = otel.Meter("gitlab.com/oss-cloud/oss-cloud/pkg/core/pool")

type Pool struct {
	m           sync.Mutex
	units       []Unit
	started     bool
	unitCounter metric.Int64UpDownCounter
	options     Options
}

func NewPool(options *Options) (_ *Pool, err error) {
	counter, err := meter.NewInt64UpDownCounter("")
	if err != nil {
		return
	}
	return &Pool{
		unitCounter: counter,
		options:     *options,
	}, nil
}

func (p *Pool) Register(u Unit) {
	p.m.Lock()
	defer p.m.Unlock()
	if p.units == nil {
		p.units = make([]Unit, 1)
	}
	p.units = append(p.units, u)

	if p.started {
		go func(u Unit) {
			_ = u.Start(context.Background())
		}(u)
	}
}

func (p *Pool) StartAndBlocking(ctx context.Context) {
	p.m.Lock()
	for _, u := range p.units {
		go func(u Unit) {
			_ = u.Start(ctx)
		}(u)
	}
	p.started = true
	p.m.Unlock()
	shutdownChan := make(chan os.Signal, 1)
	signal.Notify(shutdownChan, syscall.SIGKILL, syscall.SIGTERM)
	<-shutdownChan
	close(shutdownChan)
	var wg sync.WaitGroup
	for _, u := range p.units {
		wg.Add(1)
		go func(u Unit) {
			defer wg.Done()
			_ = u.Stop(ctx)
		}(u)
	}
	wg.Done()
}
