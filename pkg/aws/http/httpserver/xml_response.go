package httpserver

import (
	"encoding/xml"
	"io"
)

func WriteResponse(w io.Writer, response interface{}) {
	_, _ = w.Write([]byte(xml.Header))
	_ = xml.NewEncoder(w).Encode(response)
}
