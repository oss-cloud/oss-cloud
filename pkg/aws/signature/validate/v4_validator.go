package validate

import (
	"context"
	"errors"
	"net/http"
	"strings"
	"time"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/oss-cloud/oss-cloud/pkg/aws/auth"
	"gitlab.com/oss-cloud/oss-cloud/pkg/aws/signature/common"
)

var (
	ErrSignatureMismatch            = errors.New("v4: signature mismatch")
	ErrAuthHeaderEmpty              = errors.New("v4: auth header empty")
	ErrSignatureVersionNotSupported = errors.New("v4: signature version not supported")
	ErrSignatureV4InvalidFormat     = errors.New("v4: signature invalid format")
	ErrInvalidAccessKeyID           = errors.New("v4: invalid access key")
)

var v4Tracer = otel.Tracer("v4-signature-validate")

type AccessKeyLocator interface {
	Locate(accessKeyID string) (*auth.Credentials, error)
	IsAccessKeyValid(accessKeyID string) bool
}

type ValidatorV4 struct {
	Service    string
	Region     string
	KeyLocator AccessKeyLocator
	Tracer     trace.Tracer
}

func (v *ValidatorV4) Validate(r *http.Request, hashedPayload string) (cred *auth.Credentials, err error) {
	ctx, span := v.Tracer.Start(r.Context(), "Validate")
	defer span.End()

	req := *r

	authorization := req.Header.Get(common.Authorization)

	signV4Values, err := v.parse(ctx, authorization)
	if err != nil {
		return
	}

	// Extract all the signed headers along with its values.
	extractedSignedHeaders, err := extractSignedHeaders(ctx, signV4Values.SignedHeaders, r)
	if err != nil {
		return
	}

	cred, err = v.KeyLocator.Locate(signV4Values.Credential.accessKey)
	if err != nil {
		return
	}

	var date string
	if date = req.Header.Get(common.AmzDate); date == "" {
		if date = r.Header.Get(common.Date); date == "" {
			return nil, ErrSignatureV4InvalidFormat
		}
	}

	// Parse date header.
	t, e := time.Parse(common.Iso8601Format, date)
	if e != nil {
		return nil, ErrSignatureV4InvalidFormat
	}

	// Query string.
	queryStr := req.URL.Query().Encode()

	// Get canonical request.
	canonicalRequest := getCanonicalRequest(ctx, extractedSignedHeaders, hashedPayload, queryStr, req.URL.Path, req.Method)

	// Get string to sign from canonical request.
	stringToSign := common.GetStringToSign(ctx, canonicalRequest, t, signV4Values.Credential.getScope())

	// Get hmac signing key.
	signingKey := common.GetSigningKey(ctx, cred.SecretKey, signV4Values.Credential.scope.date, signV4Values.Credential.scope.region, v.Service)

	// Calculate signature.
	newSignature := common.GetSignature(ctx, signingKey, stringToSign)

	// Verify if signature match.
	if !compareSignatureV4(newSignature, signV4Values.Signature) {
		return nil, ErrSignatureMismatch
	}

	return
}

func (v *ValidatorV4) parse(ctx context.Context, authorization string) (sv *signV4Values, err error) {
	ctx, span := v.Tracer.Start(ctx, "parse")
	defer span.End()

	// credElement is fetched first to skip replacing the space in access key.
	credElement := strings.TrimPrefix(strings.Split(strings.TrimSpace(authorization), ",")[0], common.SignV4Algorithm)

	// Replace all spaced strings, some clients can send spaced
	// parameters and some won't. So we pro-actively remove any spaces
	// to make parsing easier.
	authorization = strings.Replace(authorization, " ", "", -1)
	if authorization == "" {
		return sv, ErrAuthHeaderEmpty
	}

	// Verify if the header algorithm is supported or not.
	if !strings.HasPrefix(authorization, common.SignV4Algorithm) {
		return sv, ErrSignatureVersionNotSupported
	}

	// Strip off the Algorithm prefix.
	authorization = strings.TrimPrefix(authorization, common.SignV4Algorithm)
	authFields := strings.Split(strings.TrimSpace(authorization), ",")
	if len(authFields) != 3 {
		return sv, ErrSignatureV4InvalidFormat
	}

	// Initialize signature version '4' structured header.
	signV4Values := signV4Values{}

	signV4Values.Credential, err = v.parseCredentialHeader(ctx, strings.TrimSpace(credElement))
	if err != nil {
		return sv, err
	}

	// Save signed headers.
	signV4Values.SignedHeaders, err = parseSignedHeader(ctx, authFields[1])
	if err != nil {
		return sv, err
	}

	// Save signature.
	signV4Values.Signature, err = parseSignature(ctx, authFields[2])
	if err != nil {
		return sv, err
	}

	// Return the structure here.
	return &signV4Values, nil
}

func (v *ValidatorV4) parseCredentialHeader(ctx context.Context, credElement string) (ch credentialHeader, err error) {
	ctx, span := v.Tracer.Start(ctx, "parseCredentialHeader")
	defer span.End()

	creds := strings.SplitN(strings.TrimSpace(credElement), "=", 2)
	if len(creds) != 2 {
		return ch, ErrSignatureV4InvalidFormat
	}

	if creds[0] != "Credential" {
		return ch, ErrSignatureV4InvalidFormat
	}
	credElements := strings.Split(strings.TrimSpace(creds[1]), slashSeparator)
	if len(credElements) < 5 {
		return ch, ErrSignatureV4InvalidFormat
	}
	accessKey := strings.Join(credElements[:len(credElements)-4], slashSeparator) // The access key may contain one or more `/`
	if !v.KeyLocator.IsAccessKeyValid(accessKey) {
		return ch, ErrInvalidAccessKeyID
	}

	// Save access key id.
	cred := credentialHeader{
		accessKey: accessKey,
	}
	credElements = credElements[len(credElements)-4:]
	var e error
	cred.scope.date, e = time.Parse(common.AwsDateFormat, credElements[0])
	if e != nil {
		return ch, ErrSignatureV4InvalidFormat
	}

	cred.scope.region = credElements[1]
	// Verify if region is valid.
	if v.Region != cred.scope.region {
		return ch, ErrSignatureV4InvalidFormat
	}

	if credElements[2] != v.Service {
		return ch, ErrSignatureV4InvalidFormat
	}

	cred.scope.service = credElements[2]
	if credElements[3] != "aws4_request" {
		return ch, ErrSignatureV4InvalidFormat
	}
	cred.scope.request = credElements[3]
	return cred, nil
}

// Parse signature from signature tag.
func parseSignature(ctx context.Context, signElement string) (string, error) {
	ctx, span := v4Tracer.Start(ctx, "parseSignature")
	defer span.End()

	signFields := strings.Split(strings.TrimSpace(signElement), "=")
	if len(signFields) != 2 {
		return "", ErrSignatureV4InvalidFormat
	}
	if signFields[0] != "Signature" {
		return "", ErrSignatureV4InvalidFormat
	}
	if signFields[1] == "" {
		return "", ErrSignatureV4InvalidFormat
	}
	signature := signFields[1]
	return signature, nil
}

// Parse slice of signed headers from signed headers tag.
func parseSignedHeader(ctx context.Context, signedHdrElement string) ([]string, error) {
	ctx, span := v4Tracer.Start(ctx, "parseSignedHeader")
	defer span.End()

	signedHdrFields := strings.Split(strings.TrimSpace(signedHdrElement), "=")
	if len(signedHdrFields) != 2 {
		return nil, ErrSignatureV4InvalidFormat
	}
	if signedHdrFields[0] != "SignedHeaders" {
		return nil, ErrSignatureV4InvalidFormat
	}
	if signedHdrFields[1] == "" {
		return nil, ErrSignatureV4InvalidFormat
	}
	signedHeaders := strings.Split(signedHdrFields[1], ";")
	return signedHeaders, nil
}

type signV4Values struct {
	Credential    credentialHeader
	SignedHeaders []string
	Signature     string
}

// credentialHeader data type represents structured form of Credential
// string from authorization header.
type credentialHeader struct {
	accessKey string
	scope     struct {
		date    time.Time
		region  string
		service string
		request string
	}
}

// Return scope string.
func (c credentialHeader) getScope() string {
	return strings.Join([]string{
		c.scope.date.Format(common.AwsDateFormat),
		c.scope.region,
		c.scope.service,
		c.scope.request,
	}, slashSeparator)
}
