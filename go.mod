module gitlab.com/oss-cloud/oss-cloud

go 1.15

require (
	github.com/frankban/quicktest v1.11.3 // indirect
	github.com/golang/snappy v0.0.2 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.0 // indirect
	github.com/hashicorp/go-retryablehttp v0.6.8 // indirect
	github.com/hashicorp/go-rootcerts v1.0.2 // indirect
	github.com/hashicorp/vault/api v1.0.4
	github.com/mitchellh/mapstructure v1.4.0 // indirect
	github.com/pierrec/lz4 v2.6.0+incompatible // indirect
	go.opentelemetry.io/otel v0.15.0
	golang.org/x/crypto v0.0.0-20201217014255-9d1352758620 // indirect
	golang.org/x/net v0.0.0-20201216054612-986b41b23924 // indirect
	golang.org/x/text v0.3.4 // indirect
	golang.org/x/time v0.0.0-20201208040808-7e3f01d25324 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/square/go-jose.v2 v2.5.1 // indirect
)
